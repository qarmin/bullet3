
### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://github.com/bulletphysics/bullet3.git
cd bullet3

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y libbullet-dev

cd build3
./premake4_linux64 --double gmake
cd gmake
make -j8
```

